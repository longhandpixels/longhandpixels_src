longhandpixels.net
==================

The source files for [http://longhandpixels.net/blog/](http://longhandpixels.net/blog/).

## Contributing

So you found an error, typo, bad bit of code or otherwise have a suggestion on how to improve something on LongHandPixels.net. That's great! I mean it's great you want to fix something, not that I screwed up. 

I'd love to hear your corrections/suggestions/what-have-you. Just [open an issue](https://github.com/longhandpixels/longhandpixels.net/issues/new).

If you're feeling more ambitious you can of course fork, fix and submit a pull request.

## Formatting

All these files will be processed through the [Python variant](http://pythonhosted.org/Markdown/) of [Markdown](http://daringfireball.net/projects/markdown/) with quite a few of the available extensions enabled:

* [Attribute Lists](http://pythonhosted.org/Markdown/extensions/attr_list.html)
* [Smarty](http://pythonhosted.org/Markdown/extensions/smarty.html)
* [Fenced Code Blocks](http://pythonhosted.org/Markdown/extensions/fenced_code_blocks.html)
* [Footnotes](http://pythonhosted.org/Markdown/extensions/footnotes.html)
* [Tables](http://pythonhosted.org/Markdown/extensions/tables.html)
* my own customized version of the Table of Contents plugin which outputs the ToC wrapped in `<nav>` tags instead of a `<div>`
* A slightly tweaked version of this [Figcaption Extension](https://github.com/helderco/markdown-figures/blob/master/captions.py)


If you see a bit of Markdown you don't understand, chances are it's part of one of those extensions.
