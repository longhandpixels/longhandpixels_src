---
title: How to Learn Web Development Today
pub_date: 2014-04-13 12:04:25
slug: /blog/2014/04/learn-web-development-today
metadesc: How to learn web development on the modern web or why you don't need webmonkey, A List Apart, Smashing Magazine or anyone else.

---

I launched my book today. If you're interested in learning how to build fast, user-friendly, mobile-first, progressively-enhanced responsive websites grab a copy of the awkwardly titled, ***[Build a Better Web with Responsive Web Design](https://longhandpixels.net/books/responsive-web-design?utm_source=lhp&utm_medium=blog&utm_campaign=lhp)***. There's a free 60 page excerpt you can download to try it out before you buy.

It's been a long, and lately very hectic, road from [running Webmonkey](https://longhandpixels.net/blog/2013/09/whatever-happened-to-webmonkey) to self-publishing, but along the way I got an idea stuck in my head and it won't go away: we need to embrace the post-Webmonkey world. 

To put it another way, we need to embrace the post-megasite web. Webmonkey happens to be the megasite that I identify with, but there are others as well.

I've been thinking about this in part because I've been embracing a post-megasite world on a personal level and also because several dozen people have emailed me over the past year to ask, "now that Webmonkey is gone, what publications should I follow to keep up with web development?"

Most of these people will mention A List Apart, Smashing Magazine, <strike>.Net</strike>, maybe one or two more megasites and then want to know what else is out there that they don't know about yet.

These are all great publications, but they're all, like Webmonkey, a dying breed. .Net already died in fact. There's just not much money in selling ads alongside daily web development news and web-savvy audiences like yourself tend to have ad blockers installed, which means even less money for these sites than say, CNN.

## What to Do Instead

Learning how to build websites and finding answers to your questions has become a very distributed thing in last ten years. While there are still some Webmonkey-esque things around, none of them can possibly keep track of everything. There's simply too much stuff happening too fast for any centralized resource to keep track of. Except for maybe CSS Tricks, which seems to show up in the results for pretty much every web development question I've ever had. Not sure when Chris Coyier sleeps.

For the most part though web development has long since moved past the top-down, hierarchical knowledge structures that tend to define new endeavors, when only a few people know how to do something, to a distributed model where there are so many blog posts, so many answers on Stack Overflow, so many helpful tip and tricks around the web that there is no single resource capable of tracking them all. Except Google.

This has some tremendous advantages over the world in which Webmonkey was born. 

First, it makes our knowledge more distributed, which means its more resilient. Fewer silos means less risk of catastrophic loss of knowledge. Even the loss of major things like Mark Pilgrim's wonderful HTML5 book can be recovered and distributed around so it won't disappear again.

This sort of resilience is good, but the far better part of the more distributed knowledge base is that there are so many more people to learn from. So many people facing the same situations you are and solving the same problems you have means more solutions to be found. This has a two-fold effect. First, it makes your life easier because it's easier to find answers to your questions. Second, it frees up your time to contribute back different, possibly novel solutions to the problems you can't easily answer with a bit of web searching.

So my response to people asking that question "now that Webmonkey is gone, what publications should I follow to keep up with web development?" has two parts. 

## Write What You Know

First, and most importantly, start keeping a public journal of your work. I don't mean a portfolio, I mean a record of the problems you encounter and ways you solve them. Seriously, go right now and download and install WordPress or setup GitHub Pages or something. You're a web developer. If you don't know how to do that yet, you need to do learn how to do it now. 

Now you have a website. When you get stumped by some problem start by searching to see what other people have done to solve it. Then write up the problem and post a link to the solution you find. Your future self will thank you for this; I will thank you for this. 

Not only will you have a reference point should the same question come up again, you're more likely to remember your solution because you wrote it down. 

You'll also be helping the rest of us since the more links that point to good solutions the more those solutions rise to the top of search engines. Everyone wins and you have something to refer back to in six months when you encounter the same problem again and can't remember what you did. 

For example, I don't particularly want to remember how to setup Nginx on Debian. I want to know how to do it, but I don't really want to waste actual memory on it. There are too many far more interesting things to keep track of. So, when I did sit down and figure out how to do it, I wrote a post called [Install Nginx on Debian/Ubuntu](https://longhandpixels.net/blog/2014/02/install-nginx-debian-ubuntu). Now whenever I need to know how to install Nginx I just refer to that.

## Find People Smarter Than You

My other suggestion to people looking for a Webmonkey replacement is to embrace Noam Chomsky's notion of a star system, AKA the blogroll. Remember the blogroll? That thing where people used to list the people the admired, followed, learned from? Bring that back. List the people you learn from, the people you admire, the people that are smarter than you. As they say, if you're the smartest person in the room, you're in the wrong room.  

I'll get you started, here's a list of people that are way smarter than me. Follow these people on Twitter, subscribe to their RSS feeds, watch what they're doing and don't be afraid to ask them for advice. Most of them won't bite. The worst thing that will happen is they'll ignore you, which is not the end of the world.

* [@anna_debenham](https://twitter.com/anna_debenham)
* [@yoavweiss](https://twitter.com/yoavweiss)
* [@jenville](https://twitter.com/jenville)
* [@wilto](https://twitter.com/wilto)
* [@MattWilcox](https://twitter.com/MattWilcox)
* [@aworkinglibrary](https://twitter.com/aworkinglibrary)
* [@igrigorik](https://twitter.com/igrigorik)
* [@JennLukas](https://twitter.com/JennLukas)
* [@karenmcgrane](https://twitter.com/karenmcgrane)
* [@paul_irish](https://twitter.com/paul_irish)
* [@meyerweb](https://twitter.com/meyerweb)
* [@mollydotcom](https://twitter.com/mollydotcom)
* [@lukew](https://twitter.com/lukew)
* [@susanjrobertson](https://twitter.com/susanjrobertson)
* [@adactio](https://twitter.com/adactio)
* [@Souders](https://twitter.com/Souders)
* [@lyzadanger](https://twitter.com/lyzadanger)
* [@beep](https://twitter.com/beep)
* [@tkadlec](https://twitter.com/tkadlec)
* [@brucel](https://twitter.com/brucel)
* [@johnallsopp](https://twitter.com/johnallsopp)
* [@scottjehl](https://twitter.com/scottjehl)
* [@jensimmons](https://twitter.com/jensimmons)
* [@sara_ann_marie](https://twitter.com/sara_ann_marie)
* [@samanthatoy](https://twitter.com/samanthatoy)
* [@swissmiss](https://twitter.com/swissmiss)
* [@LeaVerou](https://twitter.com/LeaVerou)
* [@stubbornella](https://twitter.com/stubbornella)
* [@brad_frost](https://twitter.com/brad_frost)
* [@chriscoyier](https://twitter.com/chriscoyier)
* [@RWD](https://twitter.com/RWD)

For simplicity's sake these are all twitter links, but have a look at the bio section in each of those links, most of them have blogs you can add to your feed reader as well. 

This list is far from complete, there are thousands and thousands of people I have not listed here, but hopefully this will get you started. Then look and see who these people are following and keep expanding your network.

Okay, now go build something you love. And don't forget to tell us how you did it.
